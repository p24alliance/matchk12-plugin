<?php
/**
 * @version 1.0 Jonah B  4/18/19 3:17 PM
 */
require_once 'vendor/autoload.php';
use iamcal\SQLParser;

$parser = new SQLParser();
$sql = file_get_contents(dirname(__FILE__)."/engagements.sql");
$parser->parse($sql);
print_r($parser->tables['engagements']['fields']);

foreach($parser->tables['engagements']['fields'] AS $field) {
    $fields[$field['name']] = $field;
}

$json = json_encode(
    $fields,
    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
);


file_put_contents(dirname(__FILE__)."/engagements.json");