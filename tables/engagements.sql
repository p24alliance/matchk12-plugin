CREATE TABLE `engagements` (
    `id` bigint(20) unsigned NOT NULL auto_increment,
    `mentor_user_id` bigint(20) NOT NULL,
    `mentee_user_id` bigint(20) NOT NULL,
    `mentor_entry_id` bigint(20) NOT NULL,
    `mentee_entry_id` bigint(20) NOT NULL,
    `date_created` datetime NOT NULL,
    `is_confirmed` tinyint(1) DEFAULT '0',
    `date_confirmed` datetime NOT NULL,
    `is_denied` tinyint(1) DEFAULT '0',
    `date_denied` datetime NOT NULL,       
    `is_completed` tinyint(1) DEFAULT '0',
    `date_completed` datetime NOT NULL,
    `rating` tinyint(2) NULL,
    `modified` timestamp DEFAULT now() ON UPDATE now(),
    `uuid` varchar(36) NULL,
    PRIMARY KEY  (`id`),
    UNIQUE INDEX `uniqueRecord` (mentor_user_id,mentee_user_id,is_confirmed,is_denied,is_completed)
)