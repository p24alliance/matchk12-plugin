<?php
/**
 * Basic PHP Wrapper for Google Data Visualization API.
 *
 * @license LGPL
 * @version Alpha $Id: $
 * @package ActiveCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */

/**
* PHP wrapper class for Google Data Visualization API.
*
* Here is an example AreaGraph time trend in conjunction with ActiveCore/DBOS framework.
*
* <code>
    if(!$range) $range = "8";
    if(!$view) $view = "weekly";
    // Column types can be 'string', 'numeric', or 'date'
    $columns = array($view=>"date","Comments"=>"number");
    $c = new SurveyResponseCommentTable();
    $row = 0;

    $graph = new GoogleGraph('commentGraph');
    $graph->addColumns($columns);

    while($row < $range) {
        $value = $c->getTrendCell($range-$row);
        // da($c);
        $graph->addValue($value,'Comments',$row);
        $graph->addValue($c->_currentDateRange['sql_starting'],$view,$row);
        $row++;
    }

    $graph->render();

    // And a pie graph with sql

    $sql = " SELECT count(*) as Hits, visitURL AS URL
        FROM evo_hitlog
        WHERE TO_DAYS(visitTime) > TO_DAYS(NOW()) - 7 ";
    // $sql .= "    AND hit_ignore = 'no' ";
    $sql .= " GROUP BY visitURL ORDER BY Hits DESC limit 10 ";
    $phits = $zdb_blog->fetchAll($sql);
    $columns = array('URL'=>'string','Hits'=>'number');
    $postGraph = new GoogleGraph('postGraph');
    $postGraph->renderTable = false;
    $postGraph->type = 'PieGraph';
    $postGraph->title = "Top 10 Posts in last 7 days";
    $postGraph->addColumns($columns);
    $postGraph->addValues($phits);
    $postGraph->render();

* </code>
*
* @author Jonah  7/21/09 11:45 AM
*/

class GoogleGraph {
    /**
    * Chart type, currently only supports AreaChart and PieChart
    */
    var $type = 'AreaChart';

    /**
    * Show table
    */
    var $renderTable = true;

    /**
    * Width
    */
    var $graphWidth = 400;
    var $graphHeight = 240;
    /**
    * view = time interval
    */
    var $view;

    /**
    * range = the number of weeks or months to display
    */
    var $range;

    /**
    * the id of the div to display the data in.
    */
    var $id;

    /**
    * Include the google base classes.
    */
    var $includeJava = true;

    /**
    * The number of rows of data.  Start at 1.
    */
    var $rowCount = 1;

    /**
    * Column names and types as label=>type associative array
    */
    var $columnNames = array();

    /**
    * spit out debugging info
    */
    var $debug = false;

    /**
    * @author Jonah
    */
    function __construct($id) {
        if(!$this->range) $this->range = 20;
        $this->id = $id;
    }

    /**
    * @param array Array of column headers, Value=>Type
    */
    function addColumns($columns) {
        $this->columnNames=$columns;
        foreach($columns AS $name=>$type) {
            $this->columnOrder[] = $name;
            switch($type) {
                case"numeric":
                    $chartType = "number";
                    $tableType = "number";
                    break;
                case"date":
                    $chartType = "string";
                    $tableType = "date";
                    break;
                default:
                    $chartType = $type;
                    $tableType = $type;
            }
            $name = $this->camelcap($name);
            $this->chartColumnHeaderString .= "  data.addColumn('{$chartType}', '{$name}'); \r\n";
            $this->tableColumnHeaderString .= "  data.addColumn('{$tableType}', '{$name}');  \r\n";
        }
        
    }
    
    function camelcap($in) {
        return $in;
    }
    
    /**
    * Takes an array to add values in a single batch.  Must be 'Label'=>'Value' array of associative arrays.
    * @author Jonah  10/6/09 2:21 PM
    */
    function addValues($data) {
        if(!is_array($data)) return false;
        foreach($data AS $row=>$record) {

            foreach($record AS $field=>$value) {
                if($fields==3) continue;
                $this->addValue($value,$field,$row);
                $fields++;
            }
            unset($fields);
        }
    }

    /**
    * Load in the data, one cell at a time.  You should also set $this->rowCount to the total rows.
    *
    * @version 1.0 Jonah  10/6/09 2:21 PM
    * @returns nothing
    */
    function addValue($value,$columnName,$rowNumber=false) {
        // For area charts you need to reverse the date order to have it go in the right direction.
        if($this->type=="AreaChart") {
            if($rowNumber>$this->rowCount) {
                $this->rowCount = $rowNumber+1;
                $noTotalSet = true;
                // We are counting as we go, don't try to reverse the order.
                $reverseCount = $rowNumber;
            } else {

                if($this->rowCount < $rowNumber + 1) $this->rowCount = $rowNumber + 1;
                $reverseCount = $this->rowCount-1;
                if($debug) echo "reverseCount was made with rowCount $this->rowCount minus rowNumber $rowNumber minus 1 <br />";

            }
        } else {
            $reverseCount = $rowNumber;
            if($this->rowCount < $rowNumber + 1) $this->rowCount = $rowNumber + 1;
        }


        if(!is_numeric($columnName)) {
            $columnNumber = $this->getColumnSequence($columnName);
        } else {
            $columnNumber = $columnName;
        }
        if($columnNumber===false) {
            echo "Could not find column name for $columnName.  use addValue(\$value,\$columnName,\$rowNumber); <br />";
            return false;
        }

        $columnType = $this->columnNames[$columnName];

        switch($columnType) {
            case"date":
                $date_ending=date("Y-m-d",strtotime($value));
                list($year,$month,$day) = explode("-",$date_ending);
                $tableday = $day;
                if($tableday<10) $tableday = str_replace("0","",$tableday);
                $tableValueString = " new Date($year,$month-1,$tableday) ";
                $chartValueString = "'$date_ending'";
                break;
            case"numeric":
            case"number":
                $tableValueString=$value;
                $chartValueString = $tableValueString;
                break;
            default:
                $chartValueString = "'{$value}'";
                $tableValueString = "'{$value}'";
        }
        $this->chartDataString .= "  data.setValue($rowNumber,$columnNumber,{$chartValueString});  \r\n";
        $this->tableDataString .= "  data.setCell($rowNumber,{$columnNumber}, {$tableValueString}); \r\n";

    }

    /**
    * @author Jonah  7/21/09 11:25 AM
    * @returns numeric sequence (starting at 0) or false if not found.
    */
    function getColumnSequence($findName) {
        $debug = false;
        foreach($this->columnOrder AS $order=>$name) {
            if($name==$findName) {
                if($debug) echo "Found $name, it's order is $order<br />";
                return $order;
            } else {
                if($debug) echo "$name did not match $findName <br />";
            }
        }
        if($debug) echo "Could not find $name did not match $findName <br />";
        return false;
    }

    /**
    * Build the javascript and optionally show the graph in the target div.
    * $this->renderTable will turn table rendering on and off.
    *
    * @author Jonah  7/29/09 9:38 AM
    */
    function render($opts=false) {
        // Be nice to people.  JB
        if($this->type=="PieGraph") $this->type = "PieChart";
        if($this->graph->width) $this->graphWidth = $this->graph->width;
        if($this->debug) echo $this->chartDataString;
        if($this->debug) da($this);
        if($this->title) { ?>
            <h2 class="GoogleGraphTitle"><?= $this->title ?></h2>
        <? } ?>
        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
          google.load('visualization', '1', {'packages':['<?= strtolower($this->type) ?>','table']});
          //google.load('visualization', '1', {'packages':['<?= strtolower($this->type) ?>']});
          google.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = new google.visualization.DataTable();
            <?= $this->chartColumnHeaderString ?>
            data.addRows(<?= $this->rowCount ?>);
            <?= $this->chartDataString ?>

            var chart = new google.visualization.<?= $this->type ?>(document.getElementById('<?= $this->id ?>_chart_div'));
            chart.draw(data, {displayAnnotations: false});
          }
        </script>
      <?php if($this->renderTable) { ?>
        <!-- now do the table -->
        <script type='text/javascript'>
          //google.load('visualization', '1', {packages:['table']});
          google.setOnLoadCallback(drawTable);
          function drawTable() {
            var data = new google.visualization.DataTable();
            <?= $this->tableColumnHeaderString ?>
            data.addRows(<?= $this->rowCount ?>);
            <?= $this->tableDataString ?>

           var table = new google.visualization.Table(document.getElementById('<?= $this->id ?>_table_div'));
           table.draw(data, {showRowNumber: true});
          }
        </script>
      <?
      } 
      ?><div id='<?= $this->id ?>_chart_div' style='width: <?= $this->graphWidth ?>px; height: <?= $this->graphHeight ?>px;'></div><?
         if($this->renderTable) {
              if($this->toggle_table) {
                $this->acToggle($this->id.'_table_div');
              }
            ?><div id='<?= $this->id ?>_table_div' <?  if($this->toggle_table) { ?>style='display:none'<? } ?>></div>
            <?php
        }
    }
    
    function acToggle($div) {
        
    }
}
