<?php
/**
* A tool for user defined and customized time/trend reports with individual columns
* as the fundamental building blocks.
*
* @liscense GPL
* @version $Id: admin_report.php 8316 2015-03-24 03:16:36Z jbaker $
* @created Jonah Baker  9/30/08 8:21 AM
* @package ActiveCore
* @subpackage ReportingEngine
* @version 1.1 JB  5/3/12 9:10 AM
*/

/**
* Do includes.
*/

require_once($_SERVER['DOCUMENT_ROOT']."/admin/includes/admin_common.php");
require_once($_SERVER['DOCUMENT_ROOT']."/action/includes/classes/class.action.php");
require_once($_SERVER['DOCUMENT_ROOT']."/admin/report/php/ACReport.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/admin/people/call/php/CallCampaign.class.php");

$action = $_REQUEST['action'];
$useBootstrap = true;
/*
* Set or delete chapter ids cookies -- SJP 01/25/2010 09:49PM
*/
if($_REQUEST['chapter_clear'] == 1) {
    chapterTable::removeChaptersSession();
} elseif($_REQUEST['chapter_id']) {
    chapterTable::setChaptersSession($_REQUEST['chapter_id'],$_REQUEST['chapter_archived']);
}

/*
* Pre-output processing
*/
switch($action) {
    case'add_chapter':
        $ct = new ChapterTable();
        $ct->choose($_REQUEST['chapter_id'],array("count"=>$_REQUEST['count'],"chapter_archived"=>$_REQUEST['chapter_archived']));
        exit;
}
$rt = new ACReportTable();
$rt->handleAjax();

/**
* Begin page output
*/

if($_REQUEST['report_id']) {
    $r = new ACReport($_REQUEST['report_id']);
    $pageTitle = $r->title;
} else {
    $pageTitle = "Report Builder";
}

if($_REQUEST['headers']!='false') show_header($pageTitle);

switch($action) {
    default:
        switch($_REQUEST['table']) {
            case'report_to_column':
                $rtc = new ReportToColumnTable();
                $rtc->adminPage();
                break;
            case'report_column':
                $column = new ReportColumnTable();
                $column->adminPage();
                break;
            default:
                $report = new ACReportTable();
                $report->adminPage();
                break;
        }
}

if($_REQUEST['headers']!='false') show_footer();


