<?php
/**
* @license GPL
* @version $Id: ReportColumnEquation.class.php 5108 2008-10-15 23:52:10Z jonah $
* @package ActiveCore
*/


/**
* @version 1.0 Jonah Baker   3/23/15 3:47 PM
* /admin/people/admin_person_attribute.php?action=attribute_summary&attribute_id=342
*/
class ReportColumnAttribute extends ReportColumn {
    /**
    * This is the preferred way to link attributes to reports, avoids id differences in dev environments.
    */
    var $attribute_code = false;
    var $attribute = false;
    var $object = false;
    var $value = false;
    
    function __construct($opts=false) {
        parent::__construct($opts);
        if($this->attribute_code) {
            $this->attribute = new Attribute($this->attribute_code);
            $this->object = new PersonAttributeTable();
            if(is_numeric($this->attribute->attribute_id)) {
                $this->object->_where = " AND attribute_id = ".$this->attribute->attribute_id;
                $this->attribute->loadStats();
            }
            // da($this->attribute);
        }
    }
    
    /**
    * HTML output for this Row column type.  
    * Currently supports + and / only, with only two columns.
    */
    function showCell($num) {
        $this->view = $this->getView();
        if(!$this->attribute->attribute_id) {
            $warning .= "No attribute exists on this server for attribute code ".$this->attribute_code." Please create it.";
        }
        if($warning) {
          ?><span class="warning"><?= $warning ?></span><?
        }
        if($this->showTotal) {
            /*
            ob_start();
            $this->value = parent::showCell($num);
            $chuck = ob_get_clean();
            */
           // $this->value = $this->attribute->count;
            // da($this->valueTotal);
            // da($this->values);
            // $this->setValue = $this->attribute->count - $this->valueTotal;
            $this->value = parent::showCell($num,array('startingTotal'=>$this->attribute->count));
        } else {
            $this->value = parent::showCell($num);
        }
        
        
        return $this->value;
        
        
        /*
        ob_start();
        $this->cell_key = $this->column_id."_".$num;

        $this->startCell();
        echo"hello".$this->attribute_code;
        if(!$this->attribute->attribute_id) {
            $warning .= "No attribute exists on this server for attribute code ".$this->attribute_code." Please create it.";
        }
        if($warning) {
          ?><span class="warning"><?= $warning ?></span><?
        }
        $this->endCell();
        echo ob_get_clean();
        return $this->value;
        */
    }
    
    /**
    * @version 1.0 Jonah B  3/30/15 3:02 PM
    */
    function makeHeaderCell() {
        // da($this->attribute);
        $divName = 'showHeaderDetail'.$this->attribute_code.''.$this->question_id;
        ob_start();
        // da($this);
        ?>
        <td class="makeHeaderCell"><?= $this->title ?> 
            <?= acToggle($divName,''); ?>
            <div style="display:none;" id="<?= $divName ?>">
            <?php if(is_object($this->attribute)) { ?>
                <?= $this->attribute->summary_link(); ?>
                <div class="caption"><?= $this->attribute->attribute_description ?></div>
            <?php } ?>
            <br />Total: <span class="badge"><?= $this->attribute->count ?></span>
            </div>
        </td><?php
        return ob_get_clean();
    }    
}
