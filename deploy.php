<?php
/**
 * git clone git@bitbucket.org:p24alliance/matchk12-plugin.git
 *
 * @version 1.0 Jonah B  4/23/19 2:48 PM
 */
$d = new PutSFTP();
// $d->mkDir('tests');
$d->putVersion();

echo("\n");
$user = 'matchk12devel-jonah';
$host = 'matchk12devel.sftp.wpengine.com';
$port = '2222';

echo("\n");

class Deploy {


    function openDir() {
        passthru('open '.dirname(__FILE__));
    }

    function copyView() {

    }
}


class PutSFTP {
    public $target = '/wp-content/plugins/matchk12-mentorships/';
    public $filesChanged = [];

    public function __construct() {
        $this->user = 'matchk12devel-jonah';
        $this->host = 'matchk12devel.sftp.wpengine.com';
        $this->port = '2222';
    }

    public function putVersion() {
        global $user,$host,$port;

        $in = r('git status');
        print_r($in);
        //$in = file_get_contents(dirname(__FILE__)."/version.txt");
        //print_r($in);
        $ls = explode("\n",$in);
        foreach($ls as $l){
            $files[] = $this->processLine($l);
        }
        foreach($this->filesChanged as $file) {
            $this->putFile($file);
        }
        $cmd = 'sftp -oPort='.$this->port.' '.$this->user.'@'.$this->host;
        echo("\n\n* ".$cmd);
        // passthru($cmd);
    }

    public function processLine($l) {
        if(strpos($l,'new file:') || strpos($l,'modified:')) {
            $parts = explode(':',$l);
            $this->filesChanged[] = trim($parts[1]);
        }
    }

    public function makeDir($dir) {
        echo("\nmkdir ".$this->target.$dir);
    }

    public function putFile($file) {
        $path = dirname(__FILE__).'/'.$file;
        $path = str_replace(' ','\ ',$path);
        $remote = $this->target.$file;
        echo("\nput ".$path." ".$remote);
    }
}

function r($in) {
 	echo("\n ".$in);
 	ob_start();
 	passthru($in);
 	$out = ob_get_clean();
 	return $out;
 }
