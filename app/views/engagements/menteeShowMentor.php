<div class="wrapMenteeShowMentor">
<!-- <h2>Hello Mentee</h2> -->

<?php
global $current_user;
global $et;

$debug = $et->getDebug();
if($debug) {
    $et->dump('hello menteeShowMentor for id '.$current_user->ID);
}
$ms = $et->findByMentee($current_user->ID);
if($debug) $et->dump($ms);

$mentorCount = 0;
if(count($ms)) {
    $m = new EngagementObject($ms[0]->id);
    $mentorCount = count($ms);
    $e = $et->getMentorEntry($m->mentor_user_id);   
}


if($debug) $et->dump('Found '.$mentorCount.' mentors for this user');

if(!$mentorCount) {
    if($debug) $et->dump('User does not have mentors.');
    
    ?>
    <div style='display:none;'>
        To engage a mentor 
        <a href="<?= $et->getMentorDirectoryUrl(); ?>" id='searchMentors'>search the mentor directory.</a>
    </div>
    <?php
    $et->showMentorship();
    ?>
<?php } else { 
    if($debug) $et->dump($m);
    if(!$m->is_confirmed && $m->id) { 
        if($debug) $et->dump('Mentorship request exists but is not confirmed');
        ?>
        <div class='alert alert-warning'>
            Your mentorship status is <b>pending</b> since 
            <b><?= date('m/d/Y',strtotime($m->date_created)); ?></b>
            <a 
                href="/<?= $et->_home ?>/?mid=<?= $m->id ?>&action=cancel" 
                class="btn btn-warning" 
                id="cancelMentorshipRequest"
            >
                Cancel Request
            </a> 
        </div>
    <?php
    } elseif($m->id) {
        // Mentorship is confirmed and active
        
        ?>
        <div class='alert alert-success'>
            Your mentorship status is <b>confirmed</b> since 
            <b><?= date('m/d/Y',strtotime($m->date_confirmed)); ?></b>.
            <?php $m->showMentorProfileLink(); ?>
            &nbsp; or &nbsp;
            <!-- 
            <a 
                href="/engagements/?mid=<?= $m->id ?>&action=cancel" 
                class="btn btn-danger" 
                id="cancelMentorshipRequest"
            >
            
            Cancel Mentorship
            </a> 
            -->
            
            &nbsp;
            <a 
                href="/<?= $et->_home ?>/?mid=<?= $m->id ?>&action=complete" 
                class="btn btn-success" 
                id="cancelMentorshipRequest"
            >
            Conclude Mentorship
            </a>             
        </div>
        <?php
    }

    if($debug) $et->da($m);
    
    
    
    ?>
        
    <?php
}
?></div><!-- end wrap_list_mentorships -->

<div class="debug" style='display:none;'>
your role(s) are  
    <?php
    foreach($current_user->roles as $role) {
        ?><b><?= $role ?></b><?php
    } ?>
</div>
<?php 