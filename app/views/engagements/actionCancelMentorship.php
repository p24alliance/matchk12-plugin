<?php
if(!$m->id) {
    // mentorship has no id, can't be found
    ?>
    <div class='alert alert-danger'>
        ERROR: This mentorship seems to have already been deleted
    </div>
    <?php
} elseif($m->mentee_user_id==$current_user->ID) {
    $m->delete();
    ?><div class='alert alert-success'>
        Mentorship request canceled.
        <a href="<?= $this->getMentorDirectoryUrl(); ?>" id="returnToDirectory">You can find another mentor now.</a>
    </div>
    <?php
} else {
    ?>
    <div class="alert alert-danger">
        ERROR: You are not authorized to cancel this mentorship, or mentorship not found. 
        Please contact support.
    </div>
    <?php
}  