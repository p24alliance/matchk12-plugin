<?php
$verified = false;
if(isset($_REQUEST['v']) && $_REQUEST['v'] == $eo->uuid) {
    $verified = true;
} 

if(
    $current_user->ID==$eo->mentor_user_id 
    || $force
    || $verified
    ) {
    
    if($eo->is_confirmed && !$force) {
        ?><div class="alert alert-warning">This mentorship has already been accepted</div><?php
    } else {
        $eo->is_confirmed = 1;
        $eo->date_confirmed = 'now';
        $eo->save('accepted');
        $mentor = get_user_by('id',$eo->mentor_user_id);
        $mentee = get_user_by('id',$eo->mentee_user_id);
        if(is_numeric($eo->id)) {
            $eo->sendMentorshipAcceptedEmailToMentor();
            $eo->sendMentorshipAcceptedEmailToMentee();
            ?><div class="alert alert-success">
                You have accepted this mentorship!  
                Both you and the mentee will receive emails shortly with contact information.
            </div><?php
        }
    }
} else {
    ?><div class='alert alert-danger'>ERROR: Mentorship accept not authorized from user or request not found</div><?php
} 