<style>
<?php require(dirname(__FILE__)."/../../../css/mentorships.css"); ?>
</style>
<div class="wrapEmail">
    <p>Dear <?= $mentor->display_name ?>,</p>
    
    <p>Good news! <?= $mentee->display_name ?> has requested help from you! </p>
    
    <p>Please review this mentee's information and accept this mentorship</p>
    
    <p>
    <a href="<?php echo WP_HOME ?><?= $this->getMenteeProfileURL("email"); ?>"><?= $mentee->display_name ?>'s Profile</a>
    </p>
    
    <p><a href="<?php echo WP_HOME ?>/<?= $this->_home ?>/?action=accept&mid=<?= (int)$this->id ?>&v=<?= $this->uuid ?>" class="btn btn-success">
        To accept the request click here.</a>
    </p>
    
    <p>
        <a href="<?php echo WP_HOME ?>/<?= $this->_home ?>/?action=decline&mid=<?= (int)$this->id ?>&v=<?= $this->uuid ?>" class="btn btn-danger">
            To decline the request click here.
        </a>
    </p>
</div>