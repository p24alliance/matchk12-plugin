<?php

if($tooManyMentees) {
    ?><div class="alert alert-danger">Sorry, this mentor already has too many mentees</div><?php
    $valid = false;
}

if($tooManyMentors 
    && !isset($_REQUEST['force'])) {
    ?><div class="alert alert-danger">Sorry, you already have a pending or active mentorship request</div><?php
    $valid = false;            
}

if($valid) {
    $debug = $_REQUEST['debug'];
    $id = $_REQUEST['id'];
    if(!is_numeric($id)) {
        $eo = new EngagementObject('new');
    } else {
        $eo = new EngagementObject($id);
    }
    if(isset($_REQUEST['mentee_id'])) {
        $eo->mentee_user_id = (int)$_REQUEST['mentee_id'];
    } else {
        $eo->mentee_user_id = $current_user->ID;
    }
    
    $eo->mentor_user_id = $mentor_id;
    $eo->date_created = 'now';
    
    $eo->save();
    
    if($debug) $eo->dump($eo);
    
    if(is_numeric($eo->id)) {
        $eo->sendMentorshipRequestEmail();
        ?><div class='alert alert-success'>SUCCESS! Your mentorship request has been sent</div><?php
    } else {
        ?>
        <div class='alert alert-danger'>
            ERROR: We could not make this mentorship request due to a system error.  
            Please contact support.
        </div>
        <?php
    }
}