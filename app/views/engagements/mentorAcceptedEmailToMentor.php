<p>Dear <?= $mentor->first_name ?></p>

<p>The contact information for your new mentee is:</p>

<p><?= strip_tags($mentee->user_email) ?></p>

<p>Good luck, and please let us know how it goes! We want to ensure that people get authentic,
meaningful help through the MatchK12 mentoring program and we'll need your 
feedback to accomplish this goal.
</p>
<p>
Thanks,<br />
Future Ready Schools Team <br />
(Sara, Tom, Lia, Avril, and Hans) <br />
</p>