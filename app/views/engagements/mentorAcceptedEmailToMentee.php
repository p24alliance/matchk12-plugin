<p>Dear <?= $mentee->first_name ?></p>

<p>Good news! Your mentorship request has been accepted!</p>

<p>Your mentor's contact information is:</p>

<p><?= strip_tags($mentor->user_email) ?></p>

<p>Good luck, and please let us know how it goes! We want to ensure that people get authentic,
meaningful help through the MatchK12 mentoring program and we'll need your 
feedback to accomplish this goal.
</p>
<p>
Thanks,<br />
Future Ready Schools Team <br />
(Sara, Tom, Lia, Avril, and Hans) <br />
</p>