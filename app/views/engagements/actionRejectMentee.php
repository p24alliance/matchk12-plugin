<?php
if($current_user->ID==$eo->mentor_user_id) {
    if($eo->is_denied) {
        ?><div class="alert alert-warning">This mentorship has already been denied</div><?php
    } else {
        $eo->is_denied = 1;
        $eo->date_denied = 'now';
        $eo->save('denied');
        
        if(is_numeric($eo->id)) {
            // $eo->sendMentorshipAcceptedEmail();
            ?><div class="alert alert-success">You have rejected this mentorship</div><?php
            if($debug) {
                $eo->dump($eo);
            }
        }
    }
} else {
    ?>
    <div class='alert alert-danger'>ERROR: Mentorship rejection request not authorized from user or request not found</div>
    <?php
} 