<?php
$debug = false;
$actives = $et->findByMentor($current_user->ID,'active');
$pendings = $et->findByMentor($current_user->ID,'pending');
$completeds = $et->findByMentor($current_user->ID,'completed');
$rejects = $et->findByMentor($current_user->ID,'denied');
$e = $et->getMentorEntry($current_user->ID);

$activeCount = count($actives);
$pendingCount = count($pendings);
$completedCount = count($completeds);
$rejectedCount = count($rejects);

?>
<!-- <h2>Hello Mentor</h2>

<a href="<?= $et->showMentorUrl($e); ?>" id="returnToMentorProfile">Return to your mentor profile</a>

-->
<div class="fr-resources">

<h2>You have  <span class="badge badge-success"><?= $activeCount ?></span> <strong>active</strong> mentees </h2>
<?php if($activeCount) { ?>
<table class="table table-striped" style="max-width:600px;">
    <thead>
        <tr>
            <th>Action</th>
            <th>Mentee</th>
            <th>Date Accepted</th>
        </tr>
    </thead>
    <tbody>
<?php

foreach($actives as $m) {
    $m = new EngagementObject($m->id);
    ?>
    <tr>
        <td><a href="/engagements/?action=reject&mid=<?= $m->id ?>" class="cancelActive">Cancel</a></td>
        <td><?= $m->showMentee(); ?>
        <td><?= $et->showDate($m->date_confirmed); ?></td>
    </tr>
    <?php
}
?>
    </tbody>
</table>
<?php } ?>

<?php // $et->da($actives); ?>

<h2>You have  <span class="badge badge-info"><?= $pendingCount ?></span> <b>pending</b> mentees </h2>

<?php if($pendingCount) { ?>
<table class="table table-striped" id='pendingMenteesTable' style="max-width:600px;">
    <thead>
        <tr>
            <th>Action</th>
            <th>Date requested</th>
            <th>Mentee</td>
        </tr>
    </thead>
    <tbody>
<?php

foreach($pendings as $p) {
    ?><tr>
        <td><?= $p->date_created ?></td>
        <td><?= $p->getMenteeUser->user_nicename ?></td>
        <td><a href="/engagements/?action=accept&mid=<?= $p->id ?>" class="approvePending">Accept</a></td>
    </tr><?php
}
?>
    </tbody>
</table>
<?php } ?>

<h2>You have  <span class="badge badge-info"><?= $completedCount ?></span> <b>completed</b> mentorships </h2>

<?php if($completedCount) { ?>
<table class="table table-striped" style="max-width:600px;">
    <thead>
        <tr>
            
            <th>Date completed</th>
            <th>Mentee</th>
        </tr>
    </thead>
    <tbody>
<?php

foreach($completeds as $m) {
    $m = new EngagementObject($m->id);
    $mentee = $m->getMenteeUser();
    
    ?><tr>
        <td><?= $m->showDateCompleted(); ?></td>
        <td><?= $mentee->user_nicename; ?></td>
    </tr><?php
}
?>
    </tbody>
</table>
<?php } ?>

</div>