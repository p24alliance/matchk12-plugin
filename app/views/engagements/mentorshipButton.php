<?php

if(
    isset($m)
    && is_object($m) 
    && isset($entry_user_id) 
    && ($entry_user_id == $m->mentee_user_id)) {
    if($m->is_confirmed) { ?>
    ?><div class="alert alert-success">This is your mentor</div><?php
    } else {
        ?><div class="alert alert-warning">
            This mentor received your mentorship request on 
            <?= date('m/d/Y',strtotime($m->date_created)); ?>
        </div><?php
    }
} elseif(
    isset($m)
    && is_object($m) 
    && $m->mentor_user_id) { 
    
    $mentorship = new EngagementObject($m->id);
    $mentor = $mentorship->getMentorUser();
    $mentorMeta = get_user_meta($m->mentor_user_id);
    // print_r($mentorMeta);
    // $mentor->dump($mentorMeta);
    /*
    $title = $mentorship->getMentorMeta('title');
    $district = $mentorship->getMentorMeta('district');
    */
    ?>
    
    <div class='mentorship mentorship-mentee-view-mentor-info'>
        <h2>Your Mentor is 
        <a href="<?= $mentorship->getMentorProfileURL(); ?>">
            <span class='mentor-name'><?= strip_tags($mentorMeta['nickname'][0]) ?></span>
        </a>
    </h2>
        <div class='mentor-title'><?= strip_tags($mentorMeta['title'][0]); ?></div>
        <div class='mentor-district'><?= strip_tags($mentorMeta['district'][0]); ?></div>
    </div>
    <!--
    <div class="alert alert-warning">You have a mentorship that is 
        pending or active, you must complete it before choosing another mentor.
    </div>
    -->
<?php 
    } elseif(strpos($_SERVER['PHP_SELF'],'mentee-view')) {
        // Looking at mentee profile, do now show button.
    } else {
        // User does not have an active or pending mentor request, show button.
        
    ?>
    <div id="wrap-mentor-button">
        <a 
            class='btn btn-warning'
            id='requestMentoshipButton'
            href="/mentorships/?action=new&tu=<?= (int)$entry_user_id ?>&to=<?= urlencode($_SERVER['REQUEST_URI']) ?>">
            Request Mentorship
        </a>
        
    </div><!-- #mentor-button -->
<?php 
} 