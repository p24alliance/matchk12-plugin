<h2>Add Mentorship</h2>

<?php echo $this->form->create($model->name, array('is_admin' => $this->is_admin)); ?>
<?php echo $this->form->open_admin_table(); ?>
<?php echo $this->form->input('mentor_user_id'); ?>
<?php echo $this->form->input('mentee_user_id'); ?>
<?php echo $this->form->input('date_created'); ?>
<?php echo $this->form->input('is_confirmed'); ?>
<?php echo $this->form->input('date_confirmed'); ?>
<?php echo $this->form->input('is_completed'); ?>
<?php echo $this->form->input('date_completed'); ?>
<?php
/*
  `mentor_user_id` bigint(20) NOT NULL,
  `mentee_user_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_confirmed` tinyint(1) DEFAULT \'0\',
  `date_confirmed` datetime NOT NULL,
  `is_completed` tinyint(1) DEFAULT \'0\',
  `date_completed` datetime NOT NULL
*/
?>
<?php echo $this->form->close_admin_table(); ?>
<?php echo $this->form->end('Add'); ?>