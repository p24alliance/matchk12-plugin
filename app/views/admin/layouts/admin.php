<?php
/**
 * @version 1.0 Jonah B  9/16/18 1:40 PM
 */
?>
<style>
<?php require(dirname(__FILE__)."/../../../../css/mentorships.css"); ?>
</style>
<?php
$et = new EngagementTable();
// $et->_createTable();
// $et->adminPage();
if(isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
} else {
    $action = false;
}
switch($action) {
    case"show":
        $e = new EngagementObject((int)$_REQUEST['id']);
        $e->showRecord();
        break;
    case"upgrade":
        $et->_updateTable20180910();
}
?>
<!--
     * total active engagements
     * average open time/time to completion
     * average open/time to completion per mentor
     * ratings of mentors
-->

<br />
<div class='mentorship-kpis' style="float:right;margin-top:10px;margin-right:10px;">

 <span class='alert alert-warning'>
        <span class="badge badge-warning"><?= $et->countByStatus('active'); ?></span> Active Engagements
    </span>
    &nbsp;
    <span class='alert alert-success'>
        <span class="badge badge-success"><?= $et->countByStatus('completed'); ?></span> Completed Engagements
    </span>
&nbsp;
    <span class='alert alert-danger'>
        <span class="badge badge-danger"><?= $et->countByStatus('pending'); ?></span> Pending Engagements
    </span>
    
</div>
<a href="admin.php?page=mvc_engagements&action=edit" class='btn btn-success' id='addNewMentorship'>
    <i class="fa fa-plus"></i> Add New
</a>
<?php
$et = new EngagementTable();
$et->adminPage();
/*
?>


<div class="wrap">
<a href="admin.php?page=mvc_engagements&action=add">Add New</a>
<?php $this->display_flash(); ?>

<?php $this->render_main_view(); ?>

</div>
<?php

*/

?>

<a href="<?= $et->_mentorViewUrl ?>">Mentor Page</a>

<p>The plugin provides the following shortcodes:
<pre>
[mentorshipbar]
[menteelist]
</pre>
<div>Connected to mentor form ID:

<a href="/wp-admin/admin.php?page=gf_entries&id=<?= $et->_mentorshipFormId ?>">
    <?= $et->_mentorshipFormId ?>
</a>
</div>

<div>
Connected to mentee form ID:
<a href="/wp-admin/admin.php?page=gf_entries&id=<?= $et->_menteeFormId ?>">
    <?= $et->_menteeFormId ?>
</a>

<div>
<a href="/wp-admin/users.php?role=mentee">List mentee users</a>

<div>
<a href="/wp-admin/users.php?role=mentor">List mentor users</a>
</div>

</div>
</p>
