<?php

require_once(dirname(__FILE__)."/../../dbos/DBOS.class.php");
require_once(dirname(__FILE__)."/../tables/EngagementTable.class.php");

/**
 * Filter the mail content type.
 */
function mentorship_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'mentorship_set_html_mail_content_type' );
 
class Engagement extends MvcModel {

    var $display_field = 'date_created';
    
    var $order = 'Engagement.date_created ASC';
    // var $includes = array('Venue', 'Speaker');
    // var $belongs_to = array('Venue');
    /*
    var $has_and_belongs_to_many = array(
        'Speaker' => array(
            'join_table' => '{prefix}events_speakers',
            'fields' => array('id', 'first_name', 'last_name')
        )
    );
    */
    
}


/**
 * Useful inherited DBOS methods:
 *   save(), delete(), showForm(), ajaxField('field_name'), formField('fieldname');
 *
 * @version 1.0 JB
 */

class EngagementObject extends EngagementTable {

    /**
    * @version 1.0 JB
    */
    function __construct($id) {
        global $wpdb;
        parent::__construct();
        
        $this->_dbo = new dboswpdb();
        $this->loadRecord($id);
    }
    
    /**
     * @version 1.0 Jonah B  9/11/18 7:14 PM
     */
    function save($status='new') {
        
        // @todo - check to see if a mentorship is pending.
        if($this->mentor_user_id && $this->contains('@',$this->mentor_user_id)) {
            // assume is an email
            $user = get_user_by('email',$this->mentor_user_id);
            $this->mentor_user_id = $user->ID;
            
        }

        if($this->mentee_user_id && $this->contains('@',$this->mentee_user_id)) {
            // assume is an email
            $user = get_user_by('email',$this->mentee_user_id);
            $this->mentee_user_id = $user->ID;
        }
        
        if(!$this->mentor_user_id) {
            ?><div class='alert alert-danger'>You must specify a mentor.  Please contact support.</div><?php
            return false;
        }
        if(!$this->mentee_user_id) {
            ?><div class='alert alert-danger'>You must specify a mentee.  Please contact support.</div><?php
            return false;
        }

        if(!$this->uuid) {
            $this->uuid = md5(date('Y-m-dH:i:s').$this->id.$this->mentor_user_id);
        }
        parent::save();
        
    }
    
    public function mentorProfileRedirect() {
        /*
        header('location: '.$this->getMentorProfileURL());
        */
        
        wp_redirect($this->getMentorProfileURL());
    }
    
    public function getMenteeProfileURL($source="web") {
        $eid = $this->getMenteeEntryId();
        $url = $this->_menteeViewUrl."/entry/".$eid;
        return $url;
    }
    
    /**
     * @version 1.0 Jonah B  10/29/18 12:10 PM
     */
    public function getMentorProfileURL($source="web") {
        $debug = false;
        $eid = $this->getMentorEntryId();
        $url = $this->_mentorViewUrl."entry/".$eid;
        if($debug) die($url);
        return $url;
    }
    
    public function getMentorEntryId($userId=false,$formId=false) {
        return parent::getMentorEntryId($this->mentor_user_id);
    }
    
    public function getMenteeEntryId($userId=false,$formId=false) {
        return parent::getMenteeEntryId($this->mentee_user_id);
    }
    
    function actionCompleteMentorship() {
        $this->is_completed = 1;
        $this->date_completed = 'now';
        $this->save();
        $this->showSuccess("Your mentorship is complete");
    }
    
    /**
     * Request from the mentor to the mentee.
     *
     * @version 1.0 Jonah B  9/10/18 3:07 PM
     */
    function sendMentorshipRequestEmail() {
        $debug = false;
        //$body = 'test mentorship email';
        
        global $current_user;
        $mentor = get_user_by('id',$this->mentor_user_id);
        $mentee = get_user_by('id',$this->mentee_user_id);
        
        if(!$current_user->ID) {
            $this->logError('sendMentorshipRequestEmail could not find the currently logged in user');
            return false;
        }
        if(!$this->mentor_user_id) {
            $this->logError('sendMentorshipRequestEmail could not find mentor_user_id');
            return false;
        }
        if(!$this->mentee_user_id) {
            $this->logError('sendMentorshipRequestEmail could not find mentee_user_id');
            return false;
        }       
        
        if(!$mentor->user_email) {
            $this->logError('sendMentorshipRequestEmail could not find mentor->user_email ');
            return false;
        }    
        
        ob_start();
        $vf = $this->getViewFile('mentorRequestEmail');
        require_once($vf);
        $body = ob_get_clean();
        
        $headers[]='Cc: jonahb0001@gmail.com';
        $subject = 'New Mentorship Request from '.$current_user->display_name;
        
        wp_mail($mentor->user_email,$subject,$body,$headers);
    }
    
    
    /**
     * This gets sent to the mentee when the mentor accepts them.
     *
     * @version 1.0 Jonah B  9/10/18 3:07 PM
     */
    function sendMentorshipAcceptedEmailToMentor() {
        $debug = false;
        //$body = 'test mentorship email';
        $mentee = get_user_by('id',$this->mentee_user_id);
        $mentor = get_user_by('id',$this->mentor_user_id);
        
        ob_start();
        $vf = $this->getViewFile('mentorAcceptedEmailToMentor');
        require_once($vf);
        
        $body = ob_get_clean();
        global $current_user;
        // The mentor's email
        // $current_user->user_email;
        /*
        $user = get_user_by( 'email', 'user@example.com' );
        echo 'User is ' . $user->first_name . ' ' . $user->last_name;
        */

        $headers[]='Cc: jonahb0001@gmail.com';
        $subject = "Contact information for your new mentorship with ".$mentee->display_name;
        wp_mail($mentor->user_email,$subject,$body,$headers);
    }
    
    /**
     * This gets sent to the mentee when the mentor accepts them.
     *
     * @version 1.0 Jonah B  9/10/18 3:07 PM
     */
    function sendMentorshipAcceptedEmailToMentee() {
        $debug = false;
        //$body = 'test mentorship email';
        $mentee = get_user_by('id',$this->mentee_user_id);
        $mentor = get_user_by('id',$this->mentor_user_id);
        
        ob_start();
        $vf = $this->getViewFile('mentorAcceptedEmailToMentee');
        require_once($vf);
        
        $body = ob_get_clean();
        global $current_user;
        // The mentor's email
        // $current_user->user_email;
        /*
        $user = get_user_by( 'email', 'user@example.com' );
        echo 'User is ' . $user->first_name . ' ' . $user->last_name;
        */

        $headers[]='Cc: jonahb0001@gmail.com';
        $subject = $mentor->display_name." accepted your mentorship request!";
        wp_mail($mentee->user_email,$subject,$body,$headers);
    }    
    
    public function getMenteeUser() {
        $m = get_user_by('id',$this->mentee_user_id);
        return $m;
    }

    
    public function getMentorUser() {
        $m = get_user_by('id',$this->mentor_user_id);
        return $m;
    }
    
    /**
     * @version 1.0 Jonah B  10/17/18 9:27 AM
     */
    public function showDateCompleted() {
        return date('m/d/Y',strtotime($this->date_completed));
    }
    
    public function showMentee() {
        $m = $this->getMenteeUser();
        ?><a href="<?= $this->getMenteeProfileURL() ?>"><?= $m->user_nicename ?></a><?php
    }
    
    public function showMentorProfileLink($entryId=false) {
        $mentor = $this->getMentorUser();
        $debug = $this->getDebug();
        if($debug) {
            $this->dump($mentor);
        }
        if(!$entryId) {
            $entryId = $this->getMentorEntry($this->mentor_user_id);
        }
        ?>
        <a 
            href="<?= $this->_mentorViewUrl ?>entry/<?= $entryId ?>"
        >
                View <span class="mentor user_nickname"><?= $mentor->data->user_nicename ?>'s</span> profile
        </a>
        <?php
    }
}

/**
 * Converter
 *
 * @version 1.0 Jonah B  9/14/18 3:56 PM
 */
class DbosWpDb  {
    
    function fetchAll($sql) {
        global $wpdb;
        return $wpdb->get_results($sql);
    }
    
    function get_result($sql) {
        global $wpdb;
        return $wpdb->get_results($sql);
    }
    function query($sql) {
        global $wpdb;
        return $wpdb->query($sql);
    }
    
    function insert($sql) {
        global $wpdb;
        return $wpdb->insert($sql);
    }    
    
    public function lastInsertId() {
        global $wpdb;
        return $wpdb->insert_id;
    }
    
    public function prepare($sql,$vars=false) {
        global $wpdb;
        return $wpdb->prepare($sql,$vars);
    }
}

