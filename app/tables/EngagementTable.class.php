<?php
/**
* [gravityview id='2224']
* Useful inherited ACTable methods:
*  adminPage - GUI for listing and editing records in the table
*  _adminShowCellValue($field,$value);
*  adminPageTopExtend - Add links to admin page navigation
*
* https://uxdesign.cc/design-better-data-tables-4ecc99d23356
*
* @version 1.0 JB 2007
*/
class EngagementTable extends DBOS {
    var $_db = "";
    var $_table = "engagements";
    var $_idField = "id";
    var $_titleField = "date_created";
    var $_dateField = 'date_created';
    var $_controller = "/wp-admin/admin.php?page=mvc_engagements";
    var $_gridEdit = false;
    var $_referToAs = 'Mentorship';
    var $_className = 'EngagementObject';
    var $_hideActionCell = true;
    var $_hideFields = array('uuid');
    
    public $_hideFilter = true;
    public $_useDataTables = true;
    public $_useBrowse = false;
    public $_allowBulkEdit = false;
    public $_showTableNavigation =false;
    
    public $_menteeLimit = 10;
    public $_menteeCount;
    
    public $_mentorshipViewId = 2224;
    public $_mentorshipFormId = 12;
    public $_mentorViewUrl = '/mentors/';
    public $_menteeViewUrl = '/view/mentee-view/';
    public $_home = 'mentorships';
    public $_menteeFormId = 15;
    public $_useUUID = true;
    
    /**
     * Still thinking about the approach
     * @version 1.0 Jonah B  9/14/18 4:49 PM
     */
    public $_fieldsJSON = '
{
    "id": {
        "name": "id",
        "type": "BIGINT",
        "length": "20",
        "unsigned": true,
        "null": false,
        "auto_increment": true
    },
    "mentor_user_id": {
        "name": "mentor_user_id",
        "type": "BIGINT",
        "length": "20",
        "null": false
    },
    "mentee_user_id": {
        "name": "mentee_user_id",
        "type": "BIGINT",
        "length": "20",
        "null": false
    },
    "date_created": {
        "name": "date_created",
        "type": "DATETIME",
        "null": false
    },
    "is_confirmed": {
        "name": "is_confirmed",
        "type": "TINYINT",
        "length": "1",
        "default": "0"
    },
    "date_confirmed": {
        "name": "date_confirmed",
        "type": "DATETIME",
        "null": false
    },
    "is_denied": {
        "name": "is_denied",
        "type": "TINYINT",
        "length": "1",
        "default": "0"
    },
    "date_denied": {
        "name": "date_denied",
        "type": "DATETIME",
        "null": false
    },
    "is_completed": {
        "name": "is_completed",
        "type": "TINYINT",
        "length": "1",
        "default": "0"
    },
    "date_completed": {
        "name": "date_completed",
        "type": "DATETIME",
        "null": false
    }
}
    ';
    
    /**
     * @version 1.0 JB  10/29/18 11:50 AM
     */
    function __construct() {
        /*
        global $zdb_;
        $this->_dbo = $zdb_;
        $this->loadTable();
        */
        global $wpdb;
        global $table_prefix;
        global $config;
        
        if($table_prefix) {
            $this->_table = $table_prefix.$this->_table;
        }
        
        $this->_dbo = new dboswpdb();
        
        $this->_gridEdit = false;
        $this->_dataTablePath = dirname(__FILE__)."/../views/admin/engagements/list.php";
        $this->_mentorDirectoryUrl = $this->_mentorViewUrl;
        
        $columns['id'] = array(
            'Field'=>'id',
            'Type'=>'bigint(20)'
        );
        
        $columns['mentor_user_id'] = array(
          'Field'=>'mentor_user_id',
          'Type'=>'bigint(20)'
        );
        
        $columns['mentee_user_id'] = array(
          'Field'=>'mentee_user_id',
          'Type'=>'bigint(20)'
        );        
        $columns['date_created'] = array(
          'Field'=>'date_created',
          'Type'=>'datetime'
        );        
        $columns['is_confirmed'] = array(
          'Field'=>'is_confirmed',
          'Type'=>'tinyint(1)',
          'Default'=>'0'
        );  
        $columns['date_confirmed'] = array(
          'Field'=>'date_confirmed',
          'Type'=>'datetime',
          'Default'=>'now'
        );          
        $columns['is_completed'] = array(
          'Field'=>'is_completed',
          'Type'=>'tinyint(1)',
          'Default'=>'0'
        );  
        $columns['date_completed'] = array(
          'Field'=>'date_completed',
          'Type'=>'datetime',
          'Default'=>'now'
        );   
        $columns['is_denied'] = array(
          'Field'=>'is_denied',
          'Type'=>'tinyint(1)',
          'Default'=>'0'
        );  
        $columns['date_denied'] = array(
          'Field'=>'date_denied',
          'Type'=>'datetime',
          'Default'=>'now'
        );    
        $columns['rating'] = array(
          'Field'=>'rating',
          'Type'=>'tinyint(2)',
          'Default'=>'0'
        );         
        $columns['uuid'] = array(
          'Field'=>'uuid',
          'Type'=>'varchar(23)',
          'Default'=>'uuid()'
        );            
        $this->_columns = $columns;
        $this->loadTable();
        
    }

    /**
     * SQL for creating this table
     *
     * @version 1.0 JB
     */
    function _createTable() {
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        global $table_prefix;
        global $wpdb;
        $sql = file_get_contents(dirname(__FILE__)."/../../tables/engagements.sql");
        echo($sql);
        dbDelta($sql);
        return $sql;
    }
    
    /**
     * @version 1.0 Jonah B  9/11/18 11:15 PM
     */
    public function _updateTable20180910() {
        global $wpdb;
        $sqls[] = "ALTER TABLE `".$wpdb->prefix."engagements` ADD PRIMARY KEY( `id`)";
        $sqls[] = 'ALTER TABLE `'.$wpdb->prefix.'engagements` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT';
        foreach($sqls AS $sql) {
            $wpdb->query($sql);
        }
    }
    
    /**
     * @version 1.0 Jonah B  9/11/18 7:10 PM
     *
     * Assume excluding completed
     */
    public function findByMentee($uid,$status=false) {
        global $wpdb;
        if(!$uid) return false;
        $debug = $this->getDebug();
        
        $sql = "SELECT * FROM ".$this->_table." 
            WHERE
                mentee_user_id = ".(int)$uid;
        switch($status) {
            default:
                $sql .= " AND is_completed = 0 ";
        }
        $res = $wpdb->get_results($sql);
        
        return $res;
    }
    
    /**
     * @version 1.0 Jonah B  9/10/18 4:09 PM
     */
    public function findByMentor($uid,$status='active') {
        global $wpdb;
        
        $sql = "SELECT * FROM ".$this->_table." 
            WHERE 1=1 
              AND  mentor_user_id = ".(int)$uid;
        $sql .= $this->filterStatus($status);

        $sql .= " ORDER BY id DESC ";
        $res = $wpdb->get_results($sql);
        
        return $res;
    }    
    
    public function filterStatus($status) {
        $sql = '';
        switch($status) {
            case'denied':
            case'rejected':
                $sql .= " AND is_denied = 1";
                break;
            case'active':
                $sql .= " AND is_confirmed = 1 AND is_denied = 0 ";
                break;
            case'pending':
                $sql .= " AND is_confirmed = 0";
                break;
            case'completed':
                $sql .= " AND is_completed = 1";
                break;
        }
        return $sql;
    }
    
    public function getDebug() {
        $debug = false;
        if($_REQUEST && isset($_REQUEST['debug'])) {
            $debug = $_REQUEST['debug'];
        }
        return $debug;
    }
    
    public function getEmailForUserId($userId) {
        $info = get_userdata($userId);
        return $info->user_email;
    }
    
    /**
     * @version 1.0 Jonah B  9/11/18 6:01 PM
     */
    public function getMentorEntry($userId,$formId=false) {
        
        $debug = false;
        $formId = $this->_mentorshipFormId;
        $email = $this->getEmailForUserId($userId);
        $debug = $this->getDebug();
        
        // for some reason 'created_by' doesn't work as a search criteria.  Iterate.  JB  9/4/18 1:43 PM
        if($debug) $this->dump('hello getMentorEntry for user id '.$userId.' and form id '.$formId. ' and email '.$email);
        
        if(!$userId) {
            if($debug) $this->dump('getMentorEntry not given user id');
            return false;
        }
        
        // Doesn't work because of imports
        // $searchCriteria['created_by'] = $userId;
        
        // This seems to give all results.  Look into doing directly to the db.
        // https://docs.gravityforms.com/api-functions/#get-entries
        $search_criteria['field_filters'][] = array( 'key' => '2', 'value' => $email );

        $es = GFAPI::get_entries($formId,$search_criteria);
        
        if($debug) $this->dump($es);
        
        foreach($es AS $e) {
            if($e['created_by'] == $userId) {
                return $e['id'];
            }
        }
        // print_r($e); 
        if($debug) $this->dump('No mentor entry found for user '.$userId.' and form id '.$formId);
        return false;
    }    
    
    /**
     * @version 1.0 Jonah B  10/29/18 12:14 PM
     */
    public function getMentorEntryId($userId,$formId=false) {
        $formId = $this->_mentorshipFormId;
        return $this->getMentorEntry($userId,$formId);
    }
    
    /**
     * @version 1.0 Jonah B  9/11/18 6:01 PM
     */
    public function getMenteeEntryId($userId,$formId=false) {
        $debug = $this->getDebug();
        
        if($debug) $this->dump('hello getMenteeEntryId');
        
        $formId = $this->_menteeFormId;
        if(!is_numeric($userId)) {
            if($debug) $this->dump('User id not found at 320');
            return false;
        }
        // for some reason 'created_by' doesn't work as a search criteria.  Iterate.  JB  9/4/18 1:43 PM
        $searchCriteria['created_by'] = $userId;
        
        $es = GFAPI::get_entries($formId,$searchCriteria);
        // if($debug) $this->da($es);
        if(!$userId) {
            if($debug) echo 'getMentorEntry not given user id';
            return false;
        }
        if($debug) {
            $this->dump($es);
        }
        foreach($es AS $e) {
            if($e['created_by'] == $userId) {
                return $e['id'];
            }
        }
        // print_r($e);    
        return false;
    }    
    
    public function showMentorUrl($entry_id) {
        ?>/view/<?= (int)$this->_mentorshipViewId ?>/entry/<?= (int)$entry_id ?><?php
    }
    
    
    /**
     * @version 1.0 Jonah B  9/11/18 8:05 PM
     */
    public function checkTooManyMentees($mid) {
        $debug = false;
        if(!$mid) {
            die('checkTooManyMentees missing id');
            return false;
        }
        $ms = $this->findByMentor($mid);
        if($debug) $this->da($ms);
        $this->_menteeCount = count($ms);
        if($this->_menteeCount>$this->_menteeLimit) {
            return true;
        } else {
            return false;
        }
    }
    
    public function checkTooManyMentors($uid) {
        if(!$uid) return false;
        $ms = $this->findByMentee($uid);
        if(count($ms)) {
            return true;
        } else {
            return false;
        }
    }    
    
    
    public function showMentorship() {
        global $current_user;
        
        if(!$current_user->ID) {
            return false;
        }
        
        require_once(dirname(__FILE__)."/../views/engagements/index.php");
        
        if($current_user) {
            $this->showMentorshipButton();
        }
    }
    
    /**
     * @version 1.0 Jonah B  9/13/18 11:23 AM
     */
    public function currentUserIsMentor() {
        global $current_user;
        $debug = false;
        return $this->getMentorEntry($current_user->ID);
    }
    /**
     * @version 1.0 Jonah B  9/13/18 11:22 AM
     */
    public function showMentorshipButton() {
        // Begin Mentor Button Logic/Display
        global $entry;
        global $entry_user_id;
        global $current_user;
        $debug = false;
        if($this->currentUserIsMentor()) {
            return false;
        }
        if($debug) $this->dump('showMentorshipButton');
        if($entry) {
            if($debug) $this->da($entry);
            
            // $entry_user = get_userdata($entry_user_id);
            $entry_user = $this->getUserFromEntry($entry);
            $entry_user_id = $entry_user->ID;
            // Is this the mentee's mentor?
            $ms = $this->findByMentee($current_user->ID);
            if(count($ms)) {
                $m = $ms[0];
                $debug = false;
                $mentees = $this->findByMentor($entry_user->ID,'active');
                $menteeCount = count($mentees);
            }
            // $entry_user_id = $entry['created_by'];
            $vf = $this->getViewFile('mentorshipButton');
            require_once($vf);
        }
        // End mentorship button logic/display
    }
    
    /**
     * form_id, date_created, created_by
     *
     * @version 1.0 Jonah B  9/13/18 10:46 AM
     */
    public function getUserFromEntry($id) {
        global $wpdb;
        $debug = false;
        $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."gf_entry WHERE id = %d",$id);
        $r = $wpdb->get_results($sql);
        // $this->da($r);
        $user = get_userdata($r[0]->created_by);
        if($debug) $this->da($user);
        return $user;
    }
    
    public function getMentorDirectoryUrl() {
        return $this->_mentorDirectoryUrl;
    }
    
    /**
     * vars $tu - target user 
     *
     * @version 1.0 Jonah B  9/12/18 7:32 PM
     */
    public function createMentorship() {
        $debug = false;
        global $current_user;
        
        if($_REQUEST['tu']) {
            $valid = true;
            // Target user has been provided.
            $target = $_REQUEST['tu'];
            if(!is_numeric($target)) {
                // assume they are using email instead of id.
                if($debug) $this->dump($target);
                $mentor_user = get_user_by('email',trim($target));
                // if($debug) $this->dump($mentor_user);
                $_REQUEST['tu']=$mentor_user->ID;
            }
            if(isset($_REQUEST['mentee_user'])) {
                // The email is being forced by admin.
                $mentee_email = $_REQUEST['mentee_user'];
                if($debug) $this->dump($mentee_email);
                $current_user = get_user_by('email',$mentee_email);
                if($debug) $this->dump($current_user);
            }
            
            if(!(int)$_REQUEST['tu']) {
                $mentor_id = $mentor_user->ID;
            } else {
                $mentor_id = (int)$_REQUEST['tu'];
            }
            if($debug) echo("\n* Mentor ID is:".$mentor_id);
            // Does the mentor have 10 mentees already?
            $tooManyMentees = $this->checkTooManyMentees($mentor_id);
            
            // Does the mentee already have a mentor request?
            $tooManyMentors = $this->checkTooManyMentors($current_user->ID);
            
            $vp = $this->getViewFile('actionCreateMentorship');
            require_once($vp);
            if($debug) print_r($eo);
            
            return true;
        } else {
            ?><div class='alert alert-danger'>ERROR: missing mentor info.  Please contact support<?php
            return false;
        }    
    }
    
    /**
     * @version 1.0 Jonah B  9/12/18 7:58 PM
     */
    public function actionAcceptMentee($mid=false) {
        $debug = false;
        global $current_user;
        $force = (int)$_REQUEST['force'];
        $et = new EngagementTable();
        $eo = new EngagementObject((int)$_REQUEST['mid']);
        if($debug) $eo->da($eo);
        $vp = $this->getViewFile('actionAcceptMentee');
        require_once($vp);  
        return $eo;
    }
    
    /**
     * @version 1.0 Jonah B  9/12/18 7:58 PM
     */
    public function actionRejectMentee($mid=false) {
        $debug = false;
        global $current_user;
        $et = new EngagementTable();
        $eo = new EngagementObject((int)$_REQUEST['mid']);
        
        if($debug) $eo->da($eo);
        $vp = $this->getViewFile('actionRejectMentee');
        require_once($vp);  
        return $eo;
    }    
    
    public function actionCancelMentorship() {
        global $current_user;
        $debug = false;
        $m = new EngagementObject((int)$_REQUEST['mid']);
        if($debug) $this->da($m);
        $vp = $this->getViewFile('actionCancelMentorship');
        require_once($vp);
        return $m;
    }
    
    /**
     * @version 1.0 Jonah B  9/18/18 12:20 AM
     */
    public function getViewFile($name) {
        global $eo;
        $debug = false;
        $themeDir = ABSPATH."wp-content/themes/generatepress/mentorships/";
        
        if(file_exists($themeDir.$name.".php")) {
            $this->_viewDir = $themeDir;
        } else {
            $this->_viewDir = dirname(__FILE__)."/../views/engagements/";
        }
        
        if($debug) $this->dump($this->_viewDir);
        $viewPath = $this->_viewDir.$name.".php";
        
        return $viewPath;
        // require_once($viewPath);
    }
    
    /**
     * @version 1.0 Jonah B  9/18/18 12:20 AM
     */
    public function dump($v) {
        $this->da($v);
    }
    
    public function show_mentor_user_id($id) {
        $user = get_user_by('id',$id);
        ob_start();
        ?><?= $user->user_email ?><?php
        return ob_get_clean();
    }    
    
    public function show_mentee_user_id($id) {
       $user = get_user_by('id',$id);
        ob_start();
        ?><?= $user->user_email ?><?php
        return ob_get_clean();    
    }
    
    public function show_date_created($date) {
        return $this->showDate($date);
    }

    public function show_date_confirmed($date) {
        return $this->showDate($date);
    }

    public function show_date_completed($date) {
        return $this->showDate($date);
    }
    public function show_date_denied($date){
        return $this->showDate($date);
    }
    public function show_is_confirmed($in) {
        return $this->showBoolean($in);
    }

    public function show_is_completed($in) {
        return $this->showBoolean($in);
    }

    public function show_is_denied($in) {
        return $this->showBoolean($in);
    }
    
    /**
     * KPIs:
     * total active engagements
     * average open time/time to completion
     * average open/time to completion per mentor
     * ratings of mentors
     */
    public function getByStatus($status) {
        global $wpdb;
        
        $sql = "SELECT * FROM ".$this->_table." 
            WHERE 1=1 ";
        $sql .= $this->filterStatus($status);

        $sql .= " ORDER BY id DESC ";
        $res = $wpdb->get_results($sql);
        
        return $res;    
    }
    
    public function countByStatus($status) {
        $res = $this->getByStatus($status);
        return count($res);
    }
    
    
    public function showBoolean($in) {
        switch($in) {
            case"0":
                return 'No';
                break;
            case"1":
                return 'Yes';
                break;
            default:
                return $in;
        }
    }
    
    public function showDate($in) {
        switch($in) {
            case"":
            case"0000-00-00 00:00:00":
                return '-';
                break;
            default:
                $out = date('m/d/Y',strtotime($in));
                return $out;
        }
    }
    
    /**
     * @var type = ('all,'mentor','mentee')
     *
     * @version 1.0 Jonah B  10/29/18 11:12 AM
     *
     */
    public function getUsers($type=false) {
        
        $sql = 'SELECT * FROM '.$this->_table." " ;
        global $wpdb;
        $ms = $wpdb->get_results($sql);
        foreach($ms AS $m) {
            $us[$m->mentee_user_id] = get_user_by('id',$m->mentee_user_id);
            $us[$m->mentor_user_id] = get_user_by('id',$m->mentor_user_id);
            
            $um[$m->mentee_user_id] = get_metadata('user',$m->mentee_user_id);
            $um[$m->mentor_user_id] = get_metadata('user',$m->mentor_user_id);
        }
        return $um;        
   
    }

}