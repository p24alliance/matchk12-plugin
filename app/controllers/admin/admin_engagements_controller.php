<?php
/**
 * @version 1.0 Jonah B  9/6/18 7:26 PM
 */
class AdminEngagementsController extends MvcAdminController {

    var $default_columns = array(
        'id', 
        'mentor_user_id',
        'mentee_user_id',
        'date_created',
        'is_confirmed',
        'date_confirmed',
        'is_completed',
        'date_completed'
    );

}
