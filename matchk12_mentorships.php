<?php
/*
Plugin Name: Matchk12 Mentorships
Version:  20200102
Plugin URI: https://bitbucket.org/p24alliance/matchk12-plugin
Text Domain: matchk12-mentorships
Description: Matchk12 Mentorship Plugin, use shortcodes 'mentorshipbar' for mentee features, and 'listmentees' for mentor profile.
Author: Jonah B
Author URI: https://www.linkedin.com/in/jonah-baker-764b585/
Bitbucket Plugin URI: p24alliance/matchk12-plugin
Bitbucket Plugin URI: https://bitbucket.org/p24alliance/matchk12-plugin
*/

/**
 * @version 1.0 Jonah B  10/29/18 12:58 PM
 * wp plugin install advanced-custom-fields
 *
 * wp plugin install https://bitbucket.org/p24alliance/matchk12-plugin/get/master.zip --force
 * wp plugin install https://github.com/afragen/github-updater/archive/develop.zip --activate
 * wp  user update mentee@anthonycimino.com --user_password=testing123
 *
 */
// https://github.com/maneuver-agency/bitbucket-wp-updater/blob/master/src/PluginUpdater.php

 require('vendor/autoload.php');
# https://github.com/maneuver-agency/bitbucket-wp-updater
$repo = 'p24alliance/matchk12-plugin';  // name of your repository. This is either "<user>/<repo>" or "<team>/<repo>".
$bitbucket_username = 'jonahwhale';   // your personal BitBucket username
$bitbucket_app_pass = 'Q7yQAcxc2Sk8UDFVcGLu';   // the generated app password with read access

// $update = new \Maneuver\BitbucketWpUpdater\PluginUpdater(__FILE__, $repo, $bitbucket_username, $bitbucket_app_pass);
// $update->initPluginData();


register_activation_hook(__FILE__, 'matchk12_mentorships_activate');
register_deactivation_hook(__FILE__, 'matchk12_mentorships_deactivate');


add_action ('wp_loaded', 'mentorship_redirects');

add_shortcode( 'mentorshipbar', 'mentorship_bar' );
add_shortcode( 'menteelist', 'mentor_list_mentees' );
add_shortcode( 'mk12profilelink', 'show_user_profile' );

add_action( 'show_user_profile', 'mentorship_additional_profile_fields' );
add_action( 'edit_user_profile', 'mentorship_additional_profile_fields' );

if(file_exists(dirname(__FILE__).'/config.php')) {
    include(dirname(__FILE__).'/config.php');
}




function mentorship_redirects() {
    $action = false;
    if($_REQUEST && isset($_REQUEST['action'])) {
        $action = strip_tags($_REQUEST['action']);
    }

    switch($action) {
        case"viewMentorProfile":
            $m = new EngagementObject((int)$_REQUEST['mid']);
            $m->mentorProfileRedirect();
            die();
    }
}

/**
 * @version 1.0 Jonah B  10/29/18 12:22 PM
 */
function matchk12_mentorships_activate() {
    global $wp_rewrite;
    require_once dirname(__FILE__).'/matchk12_mentorships_loader.php';
    $loader = new Matchk12MentorshipsLoader();
    $loader->activate();
    $wp_rewrite->flush_rules( true );
}

function matchk12_mentorships_deactivate() {
    global $wp_rewrite;
    require_once dirname(__FILE__).'/matchk12_mentorships_loader.php';
    $loader = new Matchk12MentorshipsLoader();
    $loader->deactivate();
    $wp_rewrite->flush_rules( true );
}

/**
 * [mk12profilelink]
 *
 * @version 1.0 Jonah B  5/2/19 9:48 PM
 */
function link_to_profile() {
    global $current_user;
    $et = new EngagementTable();
    $debug = $et->getDebug();

    $eid = $et->getMenteeEntryId($current_user->ID);
    if($debug) $et->dump('Hello link_to_profile for user id '.$current_user->ID.' and mentee entry id '.$eid);
    if($eid) {
    ?>
    Your <a href="<?= $et->_menteeViewUrl ?>entry/<?= $eid ?>">Mentee Profile</a>
    <?php
    }
}

/**
 * http://localhost:8085/mentorships/
 *
 * @version 1.0 Jonah B  10/16/18 12:00 AM
 */
function mentorship_bar($action=false) {
    global $current_user;
    global $et;
    $et = new EngagementTable();
    $debug = false;
    $debug = $et->getDebug();
    if($debug) {
        $et->dump('hello mentorship_bar');
        $id = $current_user->ID;

    }
    $eid = $et->getMentorEntry($current_user->ID);
    if($debug) {
        $et->dump('Your mentor entry is '.$eid);
    }

    if($debug && $eid) {
        $et->dump('You are a mentor with entry id: '.$eid);
    }

    $action = false;
    if(isset($_REQUEST['action'])) {
        $action = strip_tags($_REQUEST['action']);
    }
    if($debug) $et->dump('The action is '.$action);

    switch($action) {
        case"mk12profilelink":
            link_to_profile();
            break;
        case"viewMentorProfile":
            if($_REQUEST['mid']) {
                $mentorship = new EngagementObject((int)$_REQUEST['mid']);
            } else {
                $mentorship = $et->findByMentee($current_user->ID);
            }
            if($mentorship) {
                $mentorship->mentorProfileRedirect();
            }
            break;
        case"complete":
            $mentorship = new EngagementObject((int)$_REQUEST['mid']);
            $mentorship->actionCompleteMentorship();
            break;
        case"decline":
        case"reject":
            // mentor is rejecting mentee request
            $et->actionRejectMentee();
            break;
        case"accept":
            $et->actionAcceptMentee();
            break;
        case"cancel":
            // to cancel is to delete
            $et->actionCancelMentorship();
            break;
        case"new":
            $et->createMentorship();
            break;
        case"throwerror":
            $notAthing->doThisError();
            break;
    }

    if(!$current_user->ID) {
        // not logged in
        $vf = $et->getViewFile('notLoggedIn');
        require_once($vf);
    } elseif (!$eid) {
        // Mentee view
        $vf = $et->getViewFile('menteeShowMentor');
        require_once($vf);
    } else {
        // You are a mentor
        // mentor_list_mentees();
    }
}

/**
 * [menteelist]
 *
 * @version 1.0 Jonah B  10/22/18 9:46 AM
 */
function mentor_list_mentees() {
    global $current_user;
    $et = new EngagementTable();
    $eid = $et->getMentorEntry($current_user->ID);
    $debug = $et->getDebug();

    if($eid) {
        if(is_object($current_user)) {
            // Mentor view
            if($debug) $et->dump('You are a mentor with entry id '.$eid);
            $vf = $et->getViewFile('mentorListMentees');
            require_once($vf);
        }
    } else {
        if($debug) {
            $et->dump('hello mentor_list_mentees');
            // $et->dump($et);
        }
    }
}

/**
 * Add new fields above 'Update' button.
 *
 * @param WP_User $user User object.
 */
function mentorship_additional_profile_fields( $user ) {

    $fields = array('title','district');
    $default = false;
    $meta = get_user_meta($user->id);
    // print_r($meta);

    // $title = wp_parse_args( get_the_author_meta( 'Title', $user->ID ), $default );
    $title = $meta['title'][0];
    $default = false;

    $district = $meta['district'][0];

    print_r($district);
    ?>
    <h3>Mentorship Information</h3>
    <table class="form-table">
      <tr>
   		 <th><label for="birth-date-day">Title</label></th>
   		 <td>
   		    <input type='text' name='title' id='title' value="<?= htmlspecialchars($title); ?>" />
   		 </td>
   	 </tr>
   	 <tr>
   		 <th><label for="birth-date-day">District</label></th>
   		 <td>
   		    <input type='text' name='district' id='district' value="<?= strip_tags(htmlspecialchars($district)); ?>" />
   		 </td>
   	 </tr>
    </table>
    <?php
}

/**
 * Save additional profile fields.
 *
 * @param  int $user_id Current user ID.
 */
function mentorship_save_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }
    $debug = false;
    // print_r($_POST);
    // die();

    update_user_meta( $user_id, 'title', $_POST['title'] );
    $meta = get_user_meta($user_id);
    // print_r($meta);
    $newTitle = get_user_meta( $user_id,  'title', true );

    // print_r($newTitle);
    // die();
    if ( $_POST['title'] != $newTitle) {
        if($debug) wp_die( __( 'An error occurred setting title to '.$_POST['title'], 'textdomain' ) );
    }

    update_user_meta( $user_id, 'district', $_POST['district'] );

}

add_action( 'personal_options_update', 'mentorship_save_profile_fields' );
add_action( 'edit_user_profile_update', 'mentorship_save_profile_fields' );


if(class_exists('WP_CLI')) {
    # Register a custom 'foo' command to output a supplied positional param.
    #
    # $ wp foo bar --append=qux
    # Success: bar qux

    /**
     * My awesome closure command
     *
     * [<message>]
     * : An awesome message to display
     *
     * [--append=<message>]
     * : An awesome message to append to the original message.
     *
     * @when before_wp_load
     */
    $mentorships = function( $args=false, $assoc_args =false) {
        $et = new EngagementTable();
        $ms = $et->getUsers();
        print_r($ms);
        // WP_CLI::success( $args[0] . ' ' . $assoc_args['append'] );

    };

    WP_CLI::add_command( 'mentorships', $mentorships );


    $loadbackup = function( $args=false, $assoc_args =false) {

        $action = $args[0];
        switch($action) {
            case"changedomain":

                break;
            default:
                echo("\n* commands: changedomain");

        }
        // WP_CLI::success( $args[0] . ' ' . $assoc_args['append'] );

    };

    WP_CLI::add_command( 'loadbackup', $loadbackup );
}
/*
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/user-name/repo-name',
	__FILE__,
	'unique-plugin-or-theme-slug'
);
*/
//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
/*
$myUpdateChecker->setAuthentication(array(
	'consumer_key' => '...',
	'consumer_secret' => '...',
));
*/
//Optional: Set the branch that contains the stable release.
// $myUpdateChecker->setBranch('stable-branch-name');
