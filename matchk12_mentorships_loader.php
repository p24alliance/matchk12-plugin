<?php
/**
 * generate scaffold Matchk12Mentorships Engagement
 *
 * @version 1.0 Jonah B  9/6/18 6:31 PM
 */
 
class Matchk12MentorshipsLoader extends MvcPluginLoader {

    var $db_version = '1.1';
    var $tables = array();
    
    /**
     * Update the db schema via:
     * wp plugin deactivate matchk12-mentorships
     * wp plugin activate matchk12-mentorships
     *
     * @version 1.0 Jonah B  9/11/18 10:53 PM
     */
    function activate() {
        global $wpdb;
        // This call needs to be made to activate this app within WP MVC

        $this->activate_app(__FILE__);

        // Perform any databases modifications related to plugin activation here, if necessary

        require_once ABSPATH.'wp-admin/includes/upgrade.php';

        add_option('matchk12_mentorships_db_version', $this->db_version);

        // Use dbDelta() to create the tables for the app here
        $this->tables = array(
            'engagements' => $wpdb->prefix.'engagements',
        );
        
        $sql = '
CREATE TABLE '.$this->tables['engagements'].' (
    `id` bigint(20) unsigned NOT NULL auto_increment,
    `mentor_user_id` bigint(20) NOT NULL,
    `mentee_user_id` bigint(20) NOT NULL,
    `date_created` datetime NOT NULL,
    `is_confirmed` tinyint(1) DEFAULT \'0\',
    `date_confirmed` datetime NOT NULL,
    `is_completed` tinyint(1) DEFAULT \'0\',
    `date_completed` datetime NOT NULL,
    PRIMARY KEY  (id)
)';
        $sql = file_get_contents(dirname(__FILE__)."/tables/engagements.sql");  
        $sql = str_replace('CREATE TABLE `engagements`','CREATE TABLE `'.$wpdb->prefix.'engagements`',$sql);
        print_r($sql);
        dbDelta($sql);
        /*
$installed_ver = get_option(MY_DB_VERSION);
$wpp = $wpdb->prefix . "mypluginname";
if ($installed_ver < 102)
        $wpdb->query("ALTER TABLE ${wpp}_movies DROP nft_date");
if ($installed_ver < 107)
        $wpdb->query("ALTER TABLE ${wpp}_movies CHANGE lastupdated "
        . "lastupdated TIMESTAMP on update CURRENT_TIMESTAMP "
        . "NOT NULL DEFAULT CURRENT_TIMESTAMP");

update_option(MY_DB_VERSION, $db_version);
        */

    }

    function deactivate() {

        // This call needs to be made to deactivate this app within WP MVC

        $this->deactivate_app(__FILE__);

        // Perform any databases modifications related to plugin deactivation here, if necessary

    }

}
