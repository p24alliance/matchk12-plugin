<?php
# mentorship
# 2.3G -> 1.7G -> 700M
# brew install optipng
# find . -type f -size +1M
# sips -Z 1280 wp-content/uploads/2016/05/20160522_092036.jpg
# wp-content/uploads/2016/05/20160522_092036.jpg
# absolute path to image folder
# sips -s formatOptions 10 wp-content/uploads/2018/06/moccasins.jpg

# find wp-content/uploads/ -type f -name '*.jpg' -exec sips -Z 1024 -s formatOptions 10 {} \;


/*
2.7M	./node_modules
9.4M	./wp-admin
 36M	./wp-includes
1.6G	./wp-content
4.0K	./wp-snapshots
1.6G	.
*/
r('rm -Rf ./wp-content/ai1wm-backups/');
r("rm -Rf ./wp-content/uploads/wpallimport/");
r("rm ./wp-content/mysql.sql");
r('bzip2 *.sql');
$bigs = r('find . -type f -size +1M');
// echo($bigs);
$bs = explode("\n",$bigs);
$count = 0;
foreach($bs as $b) {
    compressFile($b);
    $count++;
}
echo "\n Files larger than 1mb: ".$count."\n";
echo r('du -d 1 -h');

function compressFile($path) {
    $deleteZips = true;
    if(!$path) return false;
    if(strpos($path,'mp4')) {
        deleteFile($path);
    } elseif(strpos($path,'.pdf')) {
        deleteFile($path);
    } elseif(strpos($path,'.sql')) {
        compressSQL($path);
    } elseif(strpos($path,'.gif')) {
        compressImage($path);
    }
    elseif(strpos($path,'.csv')) {
        deleteFile($path);
    }
    elseif(strpos($path,'.zip')) {
        if($deleteZips) {
            deleteFile($path);
        }
    }
    elseif(strpos(strtolower($path),'.jpg')) {
        compressImage($path);
    }
    elseif(strpos($path,'.jpeg')) {
        compressImage($path);
    }
    elseif(strpos($path,'.bmp')) {
        compressImage($path);
    }
    elseif(strpos($path,'.mp3')) {
        deleteFile($path);
    }
    elseif(strpos($path,'.mov')) {
        deleteFile($path);
    }
    elseif(strpos($path,'.pps')) {
        deleteFile($path);
    }
    elseif(strpos($path,'.ppt')) {
        deleteFile($path);
    }
    elseif(strpos($path,'png')) {
        compressImage($path);
    } else {
        echo "\n\n Did not know how to process file: ".$path;
        fileInfo($path);
    }
    // echo r("ls -lh ".$path);
}

function fileInfo($path) {
    echo "\n";
    echo r('ls -lh '.$path);
}

function compressImage($path) {
    $fast = true;
    $pngWidth = '800';
    echo "\n\n";
    echo r('ls -lh '.$path);
    echo r('sips -Z 1280 '.$path);
    echo r('ls -lh '.$path);
    if(strpos($path,'png')) {

        echo r('sips -Z 800 '.$path);
        if(!$fast) {
            echo r('optipng -o2 -strip all '.$path);
        }
    }
    if(strpos($path,'gif')) {
        echo r('sips -Z 800 '.$path);

    }
    echo r('ls -lh '.$path);
}
function compressSQL($path) {
    echo r('ls -lh '.$path);
}
function deleteFile($path) {
    echo "\n\n\n\n\n== Deleting: ".$path."\n";
    echo r('ls -lh '.$path);
    r('rm '.$path);
}


function r($in) {
 	// echo("\n ".$in);
 	ob_start();
 	passthru($in);
 	$out = ob_get_clean();
 	return $out;
 }
